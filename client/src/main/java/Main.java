import core.Client;

import java.io.*;
import java.net.ConnectException;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/config.properties");
        Properties properties = new Properties();
        LogManager.getLogManager().readConfiguration(
            Main.class.getResourceAsStream("logging.properties")
        );
        Logger logger = Logger.getLogger(Main.class.getName());
        properties.load(fis);
        Client client = new Client(properties, logger);
        try {
            client.initialize();
        } catch (ConnectException e) {
            System.out.println("Server socket unavailable");

            return;
        }

        logger.info("Application started");
        System.out.println(String.format("Hello!%nEnter command for client, available commands is: list, get, exit"));

        boolean exit = false;
        while (!exit) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String command = br.readLine();
            client = new Client(properties, logger);
            switch (command) {
                case "list":
                    System.out.print(client.getList());
                    break;
                case "get":
                    System.out.println("Enter filename:");
                    String filename = br.readLine();
                    System.out.println("Enter local path to save:");
                    String localPath = br.readLine();
                    try {
                        String result = client.loadFile(filename, localPath);
                        System.out.println(result);
                    } catch (RuntimeException|IOException e) {
                        System.out.println(e.getMessage());
                        System.out.println("Please retry");
                    }
                    break;
                case "exit":
                    exit = true;
                    break;
                default:
                    logger.warning(String.format("User enter wrong command - %s", command));
                    System.out.println(String.format("Unknown command!%nAvailable commands is: list, get, exit"));
            }
            client.close();
        }
    }
}
