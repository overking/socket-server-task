package core;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

public class Client {
    private InetAddress host;
    private int port;
    private Socket socket;
    private BufferedReader input;
    private PrintWriter out;
    private Logger logger;

    public Client(Properties properties, Logger logger) throws IOException {
        this.logger = logger;
        this.host = InetAddress.getByName(properties.getProperty("host", "127.0.0.1"));
        this.port = Integer.parseInt(properties.getProperty("port", "9000"));
    }

    public void initialize() throws IOException {
        this.socket = new Socket(this.host, this.port);
        this.input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.out = new PrintWriter(this.socket.getOutputStream(), true);
    }

    public String getList() throws IOException {
        this.initialize();
        this.logger.info("User execute list command");
        this.out.println("list");
        ArrayList<String> data = new ArrayList<>();
        String buf;
        while ((buf = this.input.readLine()) != null) {
            data.add(buf);
        }

        return String.join(System.lineSeparator(), data);
    }

    public String loadFile(String filename, String localPath) throws IOException {
        this.initialize();
        this.logger.info("User execute get command");

        Path path = Paths.get(localPath);
        if (localPath.isEmpty() || !Files.exists(path)) {
            this.logger.warning(String.format("Destination path \"%s\"does not exist", path));

            return "Destination path does not exist";
        }
        if (!Files.isWritable(path)) {
            this.logger.warning(String.format("Can`t write into %s", path));

            return "Can`t write into selected path";
        }

        String filePath = String.format("%s/%s", path.normalize(), filename);
        File file = new File(filePath);
        this.out.println(String.format("get%n%s", filename));
        String result = this.input.readLine();
        if (null != result && result.equals("OK")) {
            InputStream inputStream = new BufferedInputStream(this.socket.getInputStream());
            FileOutputStream writer = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int bytes = 0;
            while ((bytes = inputStream.read(buffer)) != -1) {
                writer.write(buffer, 0, bytes);
            }
        }

        return result;
    }

    public void close() throws IOException {
        if (null != this.socket) {
            this.socket.close();
        }
    }
}
