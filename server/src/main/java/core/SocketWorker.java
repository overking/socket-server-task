package core;

import service.CountLogger;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class SocketWorker extends Thread {
    private Socket socket;
    private Path storage;
    private boolean doWork;
    private BufferedReader input;
    private PrintWriter out;
    private CountLogger countLogger;

    public SocketWorker(Socket socket, Path storage, CountLogger countLogger) throws IOException{
        this.socket = socket;
        this.storage = storage;
        this.countLogger = countLogger;
        this.doWork = true;
        this.input = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        this.out = new PrintWriter(this.socket.getOutputStream(), true);;
    }

    public void run() {
        while (this.doWork || socket.isClosed()) {
            try {
                String command = this.input.readLine();
                if (null != command) {
                    switch (command) {
                        case "list":
                            this.getList();
                            break;
                        case "get":
                            String filename = this.input.readLine();
                            this.loadFile(filename);
                            break;
                        default:
                            this.out.println("Command not exist");
                    }
                    this.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public void close() throws IOException {
        this.doWork = false;
        this.socket.close();
    }

    public void loadFile(String filename) throws IOException {
        String filePath = String.format("%s/%s", this.storage, filename);
        File file = new File(filePath);
        if (!file.exists() || !file.isFile()) {
            this.out.println("File not exist of is not file");
        } else {
            this.out.println("OK");
            try {
                Thread.sleep(100); //sometimes client can late
            } catch (InterruptedException e) {
            }
            BufferedOutputStream writer = new BufferedOutputStream(this.socket.getOutputStream());
            InputStream inputStream = new FileInputStream(file);

            byte[] buffer = new byte[1024];
            int bytes = 0;
            while (( bytes = inputStream.read(buffer)) != -1) {
                writer.write(buffer, 0, bytes);
            }
            writer.flush();
            countLogger.increaseDownload(filename);
        }
    }

    private void getList() {
        StringBuilder buffer = new StringBuilder();
        try (Stream<Path> paths =  Files.walk(this.storage, 1)) { //The task have no information about depth of directory scan
            paths
                .filter(Files::isRegularFile)
                .forEach((Path file) -> {
                    buffer.append(file.getFileName() + "\n");
                });

        } catch (RuntimeException|IOException e) {
            System.out.println(e.getMessage()); //TODO use logger
        }

        this.out.println(buffer.toString());
    }
}
