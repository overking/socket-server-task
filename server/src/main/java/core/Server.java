package core;

import service.CountLogger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Logger;

public class Server {
    private InetAddress host;
    private int port;
    private int maxConnections;
    private String storagePath;
    private String countLogPath;
    private ServerSocket listener;
    private ArrayList<SocketWorker> clients;
    private Logger logger;

    public Server(Properties properties, Logger logger) throws UnknownHostException {
        this.host = InetAddress.getByName(properties.getProperty("host", "127.0.0.1"));
        this.port = Integer.parseInt(properties.getProperty("port", "9000"));
        this.maxConnections = Integer.parseInt(properties.getProperty("maxConnections", "100"));
        this.storagePath = properties.getProperty("storagePath", "/tmp");
        this.countLogPath = properties.getProperty("countLogPath", "count.log");
        this.clients = new ArrayList<>();
        this.logger = logger;
    }

    public void start() throws IOException {
        CountLogger countLogger = new CountLogger(this.countLogPath, this.logger);
        countLogger.start();

        this.listener = new ServerSocket(this.port, this.maxConnections, this.host);
        Path path = Paths.get(storagePath);
        if (!Files.exists(path)) {
            throw new IOException("Resource path does not exist");
        }
        try {
            while (true) {
                SocketWorker worker = new SocketWorker(listener.accept(), path, countLogger);
                worker.start();
                this.clients.add(worker);
            }
        }
        finally {
            for (SocketWorker client : this.clients) {
                client.close();
            }
            listener.close();
            countLogger.stopWork();
        }
    }
}
