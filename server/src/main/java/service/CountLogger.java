package service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Logger;

public class CountLogger extends Thread {
    private String path;
    private Properties properties = new Properties();
    private Logger logger;
    private boolean work = true;

    public CountLogger(String path, Logger logger) {
        this.path = path;
        this.logger = logger;
    }

    public void run() {
        while (this.work) {
            OutputStream output;
            try {
                output = new FileOutputStream(this.path);
                properties.store(output, null);
                output.close();
            } catch (IOException e) {
                this.logger.warning(e.toString());
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                this.logger.warning("Thread interrupted");
            }
        }
    }

    synchronized public void increaseDownload(String key) {
        Integer value = Integer.parseInt(this.properties.getProperty(key, "0"));
        value++;
        this.properties.setProperty(key, value.toString());
    }

    public void stopWork() {
        this.work = false;
        this.interrupt();
    }
}
