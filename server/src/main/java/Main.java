import core.Server;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/config.properties");
        Properties properties = new Properties();
        properties.load(fis);
        LogManager.getLogManager().readConfiguration(
                Main.class.getResourceAsStream("logging.properties")
        );
        Logger logger = Logger.getLogger(Main.class.getName());
        properties.load(fis);

        Server server = new Server(properties, logger);
        server.start();

    }
}
